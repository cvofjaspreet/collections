package com.staggeredgridview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.staggeredgridview.ItemView;
import com.staggeredgridview.R;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private List<ItemView> itemList;
    private Context context;

    public RecyclerViewAdapter(Context context, List<ItemView> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.solvent_list, null);
        ImageViewHolder rcv = new ImageViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {

        holder.imageView.setImageResource(itemList.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}
