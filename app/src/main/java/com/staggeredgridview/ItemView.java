package com.staggeredgridview;

public class ItemView {

    private int photo;

    public ItemView( int photo) {
        this.photo = photo;
    }

    
    public int getPhoto() {
        return photo;
    }
    
}
