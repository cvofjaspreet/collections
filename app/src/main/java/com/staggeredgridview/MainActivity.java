package com.staggeredgridview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.staggeredgridview.adapter.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);

        List<ItemView> gaggeredList = getListItemData();

        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(MainActivity.this, gaggeredList);
        recyclerView.setAdapter(rcAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        return super.onOptionsItemSelected(item);
    }

    private List<ItemView> getListItemData(){
        List<ItemView> listViewItems = new ArrayList<ItemView>();
        listViewItems.add(new ItemView(R.drawable.ic_category_travel));
        listViewItems.add(new ItemView( R.mipmap.bg_main_fb));
        listViewItems.add(new ItemView(R.drawable.ic_category_automotive));
        listViewItems.add(new ItemView(R.drawable.ic_category_fooddrink));
        listViewItems.add(new ItemView(R.drawable.ic_category_healthbeauty));
        listViewItems.add(new ItemView(R.drawable.ic_category_property));
        listViewItems.add(new ItemView(R.drawable.ic_category_shopping));
        listViewItems.add(new ItemView( R.drawable.ic_category_activities));
        return listViewItems;
    }
}
